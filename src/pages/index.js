import Layout from "@/components/UI/Layouts/Layout";
import Head from "next/head";


export default function Home() {
  return (
    <>
      <Head>
        <title>Home</title>
      </Head>
      <Layout>
        <main>
          Home
        </main >
      </Layout>
    </>

  )
}



