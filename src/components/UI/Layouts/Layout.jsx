import React from 'react'
import Link from 'next/link'
import { Routes } from '../../../utils/routes'


function Layout({ children }) {
    return (
        <section>
            <nav>
            </nav>
            {children}
        </section>
    )
}

export default Layout